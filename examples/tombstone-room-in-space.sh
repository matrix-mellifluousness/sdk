#!/bin/sh

# ====================
#
#  This script adheres to version 1.8 of the Matrix Specification:
#  https://spec.matrix.org/v1.8/client-server-api/
#
#  The Matrix Mellifluousness POSIX Shell Matrix SDK v1.1 is required to run
#  this script: https://gitlab.com/matrix-mellifluousness/sdk/-/tree/v1.1
#
# ====================

# NB: Much of this script needs to be adjusted on a case‑by‑case basis. Do not
#     attempt to use this script without modifications.

homeserver='https://matrix.arcticfoxes.net'

# Adjust path to SDK as necessary
. "$(dirname "$0")/../SDK.sh"
echo

# Existing room IDs
roomid_old='!frWBWOPyriepLexnsA:arcticfoxes.net'
roomid_space='!KnrDAAJRrUsPkFLYId:arcticfoxes.net'

# Canonical room alias (localpart only)
# NB: Must be on local homeserver
# NB: The alias indicated here must point to the old room before running the
#     script. This assumption will be rectified in a future update.
rlocalpart='PrivSec.dev'

# ====================
#
#   1. Restrict permissions in old room
#   2. Notify old room
#   3. Disconnect alias from old room
#   4. Create new room
#   5. Tombstone old room
#   6. Add new room to space
#   7. Redact old child from space
#   8. Remove old room from directory
#   9. Set restricted join rule in old room
#  10. Rename old room
#  11. Update topic of old room
#  12. Announce script completion
#  13. Request a 5-star review
#
# ====================

notify_pre 1 'Restrict permissions in old room'
e="$(send_state_event "$roomid_old" 'm.room.power_levels' '
{
  "users": {
    "@tommy:arcticfoxes.net": 100,
    "@wj25czxj47bu6q:arcticfoxes.net": 100,
    "@wj25czxj47bu6q:matrix.org": 50,
    "@raven:grapheneos.org": 99,
    "@akc3n:grapheneos.org": 50,
    "@akc3n:arcticfoxes.net": 50,
    "@madaidan.:matrix.org": 50,
    "@mjolnir:arcticfoxes.net": 49
  },
  "users_default": 0,
  "events": {
    "m.room.server_acl": 49,
    "m.room.encryption": 9007199254740991,
    "m.room.create": 9007199254740991,
    "m.space.child": 9007199254740991,
    "m.policy.rule.user": 9007199254740991,
    "m.policy.rule.room": 9007199254740991,
    "m.policy.rule.server": 9007199254740991,
    "m.call.invite": 9007199254740991,
    "m.call.answer": 9007199254740991,
    "m.call.candidates": 9007199254740991,
    "org.matrix.msc3401.call": 9007199254740991,
    "org.matrix.msc3401.call.member": 9007199254740991
  },
  "events_default": 100,
  "state_default": 100,
  "redact": 100,
  "invite": 50,
  "kick": 49,
  "ban": 49,
  "notifications": { "room": 100 }
}
')"
notify_post "$e"
sleep 0



notify_pre 2 'Notify old room'
e_old_notice="$(request 'PUT' "/v3/rooms/$roomid_old/send/m.room.message/$(txnId)" 'event_id' '
{
  "body": "** This room is hereby being tombstoned and replaced. **\n\nYour client should provide a link to join the new room. If not, manually join the replacement at #'"$rlocalpart:$hs_name"'.\n\nFluffyChat is known to handle tombstoning ungracefully. Element desktop/mobile, SchildiChat desktop/mobile, and Cinny are known to work.\n\nIn case of any unforeseen issues, please be patient and check back here after a few minutes.",
  "format": "org.matrix.custom.html",
  "formatted_body": "<h3>This room is hereby being <em>tombstoned</em> and replaced.</h3>\n<p>Your client should provide a link to join the new room. If not, manually join the replacement at <a href=\"matrix:r/'"$rlocalpart:$hs_name"'\">#'"$rlocalpart:$hs_name"'</a>.</p>\n<p><strong>FluffyChat is known to handle tombstoning ungracefully.</strong> Element desktop/mobile, SchildiChat desktop/mobile, and Cinny are known to work.</p>\n<p><em>In case of any unforeseen issues, please be patient and check back here after a few minutes.</em></p>",
  "msgtype": "m.notice"
}
')"
notify_post "$e_old_notice"
sleep 0



notify_pre 3 'Disconnect alias from old room'
request 'DELETE' "/v3/directory/room/%23$rlocalpart%3A$hs_name"
notify_post
sleep 0



notify_pre 4 'Create new room'
roomid_new="$(request 'POST' '/v3/createRoom' 'room_id' '
{
  "power_level_content_override": {
    "users": {
      "@tommy:arcticfoxes.net": 100,
      "@wj25czxj47bu6q:arcticfoxes.net": 100,
      "@wj25czxj47bu6q:matrix.org": 50,
      "@raven:grapheneos.org": 99,
      "@akc3n:grapheneos.org": 50,
      "@akc3n:arcticfoxes.net": 50,
      "@madaidan.:matrix.org": 50,
      "@mjolnir:arcticfoxes.net": 49
    },
    "users_default": 0,
    "events": {
      "m.room.message": 0,
      "m.room.encrypted": 100,
      "m.sticker": 0,
      "m.reaction": 0,
      "m.room.redaction": -9007199254740991,
      "org.matrix.msc3381.poll.start": 0,
      "org.matrix.msc3381.poll.response": 0,
      "org.matrix.msc3381.poll.end": 0,
      "m.room.pinned_events": 50,
      "im.ponies.room_emotes": 10,
      "m.room.name": 95,
      "m.room.avatar": 50,
      "m.room.topic": 50,
      "m.room.canonical_alias": 95,
      "m.room.power_levels": 50,
      "m.room.history_visibility": 100,
      "m.room.join_rules": 49,
      "m.room.guest_access": 100,
      "m.room.server_acl": 49,
      "m.room.encryption": 9007199254740991,
      "m.room.tombstone": 100,
      "m.space.child": 9007199254740991,
      "m.space.parent": 100,
      "m.policy.rule.user": 9007199254740991,
      "m.policy.rule.room": 9007199254740991,
      "m.policy.rule.server": 9007199254740991,
      "m.call.invite": 9007199254740991,
      "m.call.answer": 9007199254740991,
      "m.call.candidates": 9007199254740991,
      "m.call.hangup": -9007199254740991,
      "org.matrix.msc3401.call": 9007199254740991,
      "org.matrix.msc3401.call.member": 9007199254740991,
      "org.matrix.room.preview_urls": 100,
      "im.vector.modular.widgets": 100
    },
    "events_default": 25,
    "state_default": 100,
    "redact": 49,
    "invite": 50,
    "kick": 49,
    "ban": 49,
    "notifications": { "room": 95 }
  },
  "invite": [],
  "room_version": "10",
  "visibility": "public",
  "name": "PrivSec.dev — Practical Privacy & Security",
  "topic": "Technical discussions only. No politics, no conspiracy theories, no \"backdoored because proprietary\". Any claim of malware/spyware must be backed up with technical proof. ✱✱Send fewer, longer messages instead of many short messages to avoid getting caught by the spam filter.✱✱\n\nhttps://privsec.dev\n\nOff‑topic: #PrivSec.dev-offtopic:arcticfoxes.net\nSpace: #PrivSec.dev-space:arcticfoxes.net",
  "room_alias_name": "'"$rlocalpart"'",
  "creation_content": {
    "predecessor": {
      "room_id": "'"$roomid_old"'",
      "event_id": "'"$e_old_notice"'"
    },
    "m.federate": true
  },
  "initial_state": [
    {
      "type": "m.room.avatar",
      "content": {
        "url": "mxc://arcticfoxes.net/dHuSXBdyAqgbDoQVyhJQgSfS"
      }
    },
    {
      "type": "m.room.join_rules",
      "content": {
        "join_rule": "public"
      }
    },
    {
      "type": "m.room.guest_access",
      "content": {
        "guest_access": "forbidden"
      }
    },
    {
      "type": "m.room.history_visibility",
      "content": {
        "history_visibility": "world_readable"
      }
    },
    {
      "type": "m.space.parent",
      "state_key": "'"$roomid_space"'",
      "content": {
        "canonical": true,
        "via": [
          "arcticfoxes.net",
          "matrix.org",
          "tchncs.de"
        ]
      }
    }
  ]
}
')"
notify_post "$roomid_new"
sleep 0



notify_pre 5 'Tombstone old room'
e="$(send_state_event "$roomid_old" 'm.room.tombstone' '
{
  "body": "This room has been replaced by #'"$rlocalpart:$hs_name"'",
  "replacement_room": "'"$roomid_new"'"
}
')"
notify_post "$e"
sleep 0



notify_pre 6 'Add new room to space'
e="$(send_state_event "$roomid_space" 'm.space.child' "$roomid_new" '
{
  "suggested": true,
  "order": "a",
  "via": [
    "arcticfoxes.net",
    "matrix.org"
  ]
}
')"
notify_post "$e"
sleep 0



notify_pre 7 'Redact old child from space'
e="$(send_state_event "$roomid_space" 'm.space.child' "$roomid_old" '{}')"
notify_post "$e"
sleep 0



notify_pre 8 'Remove old room from directory'
request 'PUT' "/v3/directory/list/room/$roomid_old" '' '{ "visibility": "private" }'
notify_post
sleep 0



notify_pre 9 'Set restricted join rule in old room'
e="$(send_state_event "$roomid_old" 'm.room.join_rules' '
{
  "join_rule": "restricted",
  "allow": [
    {
      "room_id": "'"$roomid_new"'",
      "type": "m.room_membership"
    }
  ]
}
')"
notify_post "$e"
sleep 0



notify_pre 10 'Rename old room'
e="$(send_state_event "$roomid_old" 'm.room.name' '{ "name": "[OLD] PrivSec.dev — Practical Privacy & Security" }')"
notify_post "$e"
sleep 0



notify_pre 11 'Update topic of old room'
e="$(send_state_event "$roomid_old" 'm.room.topic' '{ "topic": "Superseded by #'"$rlocalpart:$hs_name"'" }')"
notify_post "$e"
sleep 0



notify_pre 12 'Announce script completion'
e_glory="$(request 'PUT' "/v3/rooms/$roomid_new/send/m.room.message/$(txnId)" 'event_id' '
{
  "body": "GLORIOUS TOMBSTONE SCRIPT COMPLETED!\n\nPlease rate your experience: ☆ ☆ ☆ ☆ ☆",
  "format": "org.matrix.custom.html",
  "formatted_body": "<strong><a href=\"https://gitlab.com/matrix-mellifluousness/sdk/-/blob/ec678e2a/examples/tombstone-room-in-space.sh\" target=\"_blank\">GLORIOUS TOMBSTONE SCRIPT</a> COMPLETED!</strong><br><br>Please rate your experience: <span data-mx-color=\"#ffd700\">☆ ☆ ☆ ☆ ☆</span>",
  "msgtype": "m.notice"
}
')"
notify_post "$e_glory"
sleep 0



notify_pre 13 'Request a 5-star review'
e="$(request 'PUT' "/v3/rooms/$roomid_new/send/m.reaction/$(txnId)" 'event_id' '
{
  "m.relates_to": {
    "event_id": "'"$e_glory"'",
    "key": "⭐⭐⭐⭐⭐",
    "rel_type": "m.annotation"
  }
}
')"
notify_post "$e"



# Finish
echo
echo 'Tombstone script successfully completed.'
echo
