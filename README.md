# POSIX Shell Matrix SDK

This script provides wrappers around `curl` and `jq` to help reduce boilerplate and implement basic error handling for shell scripts targeting the [Matrix Client‑Server API](https://spec.matrix.org/v1.8/client-server-api/).

[curl](https://curl.se/) and [jq](https://jqlang.github.io/jq/) must be installed to run this script. They can be found in virtually every Linux distribution's repositories.

_Breaking changes will always be indicated by a bump in major version number, but a bump in major version number does not necessarily indicate breaking changes. Refer to the [release notes](https://gitlab.com/matrix-mellifluousness/sdk/-/releases)._

## Known Limitations

- Ratelimit handling is not currently implemented — ratelimiting will be treated like any other error and halt script execution. If a server‑side ratelimiting exemption is not available, liberal use of `sleep` commands is encouraged.

  - Note that the SDK will automatically send one unauthenticated request (`GET /_matrix/client/v3/login`) and one authenticated request (`GET /_matrix/client/v3/account/whoami`) on startup.

## Required Boilerplate

```sh
#!/bin/sh

homeserver='https://matrix-client.matrix.org'

. "/path/to/SDK.sh"
```

`homeserver` is the exact URL (and port if not 443) where the Matrix homeserver listens for the client‑server API. _It can be found in Element Settings > Help & About._

[Dot](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#dot) the SDK to make its commands available for use.

The SDK will prompt for the access token to use. If non‑interactive usage is necessary, the access token may be specified as an environment variable `ACCESS_TOKEN`. _The access token can be found in Element Settings > Help & About._

## Command Reference

<dl><dt>

`notify_pre 0 'description'`

</dt><dd>

_Used for logging purposes in combination with `notify_post`._

Writes an integer step number and brief description of an action in the script to standard output.

Execute `notify_pre` **immediately before** the the action in question.

</dd><dt>

`notify_post ['identifier']`

</dt><dd>

_Used for logging purposes in combination with `notify_pre`._

Writes '✔️' and optional text to standard output to indicate successful completion of an action in the script. The text is recommended to be a relevant Matrix identifier returned by the action in [common identifier format](https://spec.matrix.org/v1.8/appendices/#common-identifier-format), such as the event ID after sending an event or the room ID after creating a room.

Execute `notify_post` **immediately after** the the action in question.

</dd><dt>

`request 'GET/POST/PUT/DELETE/OPTIONS' '/v0/endpoint' ['target key' ['request body']]`

</dt><dd>

Performs an arbitrary API call with specified request method and optional JSON request body to the specified endpoint beginning with implicit `/_matrix/client`. The response JSON is parsed for a particular top‑level key if specified, whose value is returned on standard output. _The target key may be specified as an empty string `''` to supply a request body without parsing the response._

</dd><dt>

`send_state_event 'roomId' 'eventType' ['stateKey'] 'content'`

</dt><dd>

Sends a state event to a room. Refer to [upstream documentation](https://spec.matrix.org/v1.8/client-server-api/#put_matrixclientv3roomsroomidstateeventtypestatekey).

</dd><dt>

`txnId`

</dt><dd>

Generates a (hopefully unique) transaction ID by prefixing the current Unix time with the string "MMSDK" and returns it on standard output.

</dd></dl>

## Usage Examples

- [Tombstone and replace a room in a space](examples/tombstone-room-in-space.sh)