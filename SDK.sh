#!/bin/sh

# ====================
#
#  Matrix Mellifluousness POSIX Shell Matrix SDK v1.0
#
#  This script adheres to version 1.8 of the Matrix Specification: https://spec.matrix.org/v1.8/client-server-api/
#
#  curl and jq must be installed to run this script. Refer to README.md for usage instructions.
#
# ====================

set -e

# Check dependencies
test -n "$(curl --version 2>&1 1> /dev/null)" && echo 'curl not found, aborting…' >&2 && exit 1
test -n "$(jq --version 2>&1 1> /dev/null)" && echo 'jq not found, aborting…' >&2 && exit 1

# Retrieve access token
access_token="$ACCESS_TOKEN"
test -n "$access_token" || { printf 'Please enter your access token: ' && IFS= read -r access_token ;}
test -n "$access_token" || { echo 'Access token not provided, aborting…' >&2 && exit 1 ;}

# ====================

# notify_pre 0 'description'
notify_pre() {
  printf '%2d. %-46s  ' "$1" "$2"
}

# notify_post ['identifier']
notify_post() {
  test -n "$1" && printf '✔️  %s\n' "$1" || printf '✔️\n'
}

# request 'GET/POST/PUT/DELETE/OPTIONS' '/v0/endpoint' ['target key' ['request body']]
request() {
  localvar_endpoint="/_matrix/client$2"
  localvar_response="$(printf '%s' "$4" | jq -c | curl -sS -X "$1" -H "Authorization: Bearer $access_token" "$homeserver$localvar_endpoint" --json @-)" || { echo "⚠️  Critical error in request to $localvar_endpoint ⚠️" >&2 && exit 1 ;}
  printf '%s' "$localvar_response" | jq -e '.errcode' >/dev/null && echo "$localvar_response" >&2 && echo "⚠️  Critical error in request to $localvar_endpoint ⚠️" >&2 && exit 1
  if [ -n "$3" ]; then
    printf '%s' "$localvar_response" | jq -r "if has(\"$3\") then .$3 else halt_error end" || { echo "⚠️  Critical error parsing value of key \"$3\" in response from $localvar_endpoint ⚠️" >&2 && exit 1 ;}
  fi
}

# send_state_event 'roomId' 'eventType' ['stateKey'] 'content'
send_state_event() {
  localvar_stateKey="${4:+$3}"
  localvar_content="${4:-$3}"
  request 'PUT' "/v3/rooms/$1/state/$2/$localvar_stateKey" 'event_id' "$localvar_content"
}

# txnId
txnId() {
  printf 'MMSDK%s' "$(awk 'BEGIN{srand();print srand()}')" # https://stackoverflow.com/a/12746260
}

# ====================

# Verify we are talking to a real Matrix server
if [ "$(curl -sS -H 'Accept: application/json' "$homeserver/_matrix/client/v3/login" --output /dev/null -w '%{http_code}')" != '200' ]; then
  echo 'Failed to communicate with homeserver, aborting…' >&2
  exit 1
fi

# Authenticate ourselves
u_me="$(request 'GET' '/v3/account/whoami' 'user_id')" || { printf '\a\n🚨 YOUR ACCESS TOKEN MAY HAVE BEEN LEAKED. SIGN OUT OF THAT SESSION IMMEDIATELY. 🚨\n\n' >&2 && exit 1 ;}
hs_name="$(echo "$u_me" | cut -d ':' -f 2)"
echo "Successfully authenticated as $u_me"
